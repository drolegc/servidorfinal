package servicios;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.ws.Endpoint;

import uytube.models.Video;
import uytube.VideoController.*;

@WebService
@SOAPBinding(style = Style.RPC, parameterStyle = ParameterStyle.WRAPPED)
public class Test {

	private Endpoint endpoint;
	
	public Test() {}
	
	@WebMethod(exclude = true)
	public void publicar() {
		endpoint = Endpoint.publish("http://localhost:9874/test", this);
	}
	
	@WebMethod(exclude = true)
	public Endpoint getEndpoint() {
		return endpoint;
	}
	
	/*
	 * Ejemplo devolviendo un objeto
	 * */
	
	@WebMethod
	public Video algunVideo(int id) {
		//Testeo de obtener un video por id
		
		IVideo controller = new VideoController();
		Video video_a_retornar = controller.consultaVideoPorID(id);
		return video_a_retornar;
	}
	
	@WebMethod
	public String devolviendoString() {
		return "Prueba de testeo";
	}
}